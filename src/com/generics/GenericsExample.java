package com.generics;

import java.util.ArrayList;

public class GenericsExample<T> {

	T ob;
	public GenericsExample(T ob) {
		this.ob=ob;
	}
	public void show() {
		System.out.println("The type of OB is "+ob.getClass().getName());
	}
	public T getOb() {
		return ob;
	}
}
