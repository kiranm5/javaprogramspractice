package com.generics;

public class BoundedGenerics<T extends Number> {  
  
	T print;
	public BoundedGenerics(T print) {
		this.print=print;
	}
    public void print() {
    	System.out.println(print);
        }  
  
    public static void main(String[] args) {  
    	BoundedGenerics<Integer> in = new BoundedGenerics<>(5);  
        in.print();
  
        BoundedGenerics<Double> d = new BoundedGenerics<>(5.5);  
        d.print(); 
        
        BoundedGenerics<Float> f = new BoundedGenerics<>(15.5f);  
        f.print(); 
    }  
}  
