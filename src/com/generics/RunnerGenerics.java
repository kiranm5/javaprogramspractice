package com.generics;

public class RunnerGenerics {

	public static void main(String[] args) {
		GenericsExample<String> g= new GenericsExample<String>("Kiran");
		g.show();
		System.out.println(g.getOb());
		GenericsExample<Integer> g1= new GenericsExample<Integer>(99);
		g1.show();
		System.out.println(g1.getOb());

	}

}
