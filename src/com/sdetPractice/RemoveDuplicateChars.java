package com.sdetPractice;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class RemoveDuplicateChars {

	public static void main(String[] args) {
		String input= "Bangalore";
		char ch[]=input.toCharArray();
		HashSet<Character> lhs=new LinkedHashSet<Character>();
		for(char c:ch) {
			lhs.add(c);
		}
		System.out.println(lhs);
	}
}
