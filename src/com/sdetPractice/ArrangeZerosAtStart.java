package com.sdetPractice;

public class ArrangeZerosAtStart {

	public static void main(String[] args) {
		int arr[]= {0,9,5,6,7,3,0,6};
		int len=arr.length;
		int nonZeroIndex=0;
		for(int index=0;index<len;index++) {
			if(arr[index]==0) {
				int temp=arr[index];
				arr[index]=arr[nonZeroIndex];
				arr[nonZeroIndex]=temp;
				nonZeroIndex++;
			}
		}
		for(int index=0;index<len;index++) {
			System.out.println(arr[index]);
		}
		

	}

}
