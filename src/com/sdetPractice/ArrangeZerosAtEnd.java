package com.sdetPractice;

public class ArrangeZerosAtEnd {

	public static void main(String[] args) {
		int arr[]= {1,0,4,5,0,7};
		int len=arr.length;
		int nonZeroIndex=0;
		
		for(int index=0;index<len;index++) {
			if(arr[index]!=0) {
				int temp=arr[index];
				arr[index]=arr[nonZeroIndex];
				arr[nonZeroIndex]=temp;
				nonZeroIndex++;
			}
		}
		
		for(int index=0;index<len;index++) {
			System.out.println(arr[index]);
		}

	}

}
