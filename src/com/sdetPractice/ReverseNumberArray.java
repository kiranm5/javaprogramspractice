package com.sdetPractice;

import java.util.Arrays;

public class ReverseNumberArray {

	public static void main(String[] args) {
		int arr[]= {3,4,5,6,7,9};
		int left=0;
		int right=arr.length-1;
		while(left!=right&&left<=right)
		{
			int temp=arr[left];
			arr[left]=arr[right];
			arr[right]=temp;
			right=right-1;
			left=left+1;
		}
		System.out.println(Arrays.toString(arr));
	}

}
