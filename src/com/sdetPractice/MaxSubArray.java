package com.sdetPractice;

public class MaxSubArray {

	public static void main(String[] args) {
		int a[]= {-2,4,5,-2,8,9,-5};
		int len=a.length-1;
		int currentSum=a[0];
		int maxSum=a[0];
		for(int i=1;i<=len;i++) {
			if(a[i]>currentSum+a[i]) {
				currentSum=a[i];
			}
			else {
				currentSum+=a[i];
			}
			if(currentSum>maxSum) {
				maxSum=currentSum;
			}
		}
		System.out.println(maxSum);
	}

}
