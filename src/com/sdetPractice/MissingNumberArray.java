package com.sdetPractice;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class MissingNumberArray {
	
	public static void main(String[] args) {
		int num[]= {5,7,8,9,2};
		Arrays.sort(num);
		System.out.println("Sorted Array: ");
		for (int n : num)
		{
			System.out.print(" "+n);
		}
		System.out.println();
		int len=num.length;
		int min=num[0];
		int max=num[len-1];
		HashSet<Integer> lhs=new HashSet<Integer>();
		for (int n : num)
		{
			lhs.add(n);
		}
		System.out.println("Hashset elements "+lhs);
		System.out.println("Missing elements");
		for(int i=min;i<=max;i++) {
			if(!lhs.contains(i))
			{
				System.out.println(i);
			}
		}
		
	}
}