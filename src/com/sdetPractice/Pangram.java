package com.sdetPractice;

import java.util.Set;
import java.util.TreeSet;

public class Pangram {

	public static void main(String[] args) {
		String s="The quick brown fox jumps over a lazy dog";
		s=s.toLowerCase().replace(" ","");
		char[] ch=s.toCharArray();
		Set<Character> ts=new TreeSet<Character>();
		for(char c:ch) {
			ts.add(c);
		}
		System.out.println(ts);
	}
}
