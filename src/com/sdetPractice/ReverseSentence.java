package com.sdetPractice;

public class ReverseSentence {

	public static void main(String[] args) {
		String str="my name is Kiran";
		String[] words=str.split(" ");
		int wordsLength=words.length-1;
		for(int i=wordsLength;i>=0;i--) {
			System.out.print(words[i]);
			System.out.print(" ");
		}

	}

}
