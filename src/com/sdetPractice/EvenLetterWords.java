package com.sdetPractice;

public class EvenLetterWords {

	public static void main(String[] args) {
		
		String s="Sky is blue and vast";
		String[] word=s.split(" ");
		for(String w:word) {
			if(w.length()%2==0) {
				System.out.println(w);
			}
		}

	}

}
