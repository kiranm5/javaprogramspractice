package com.sdetPractice;

public class ReverseEachWord {

	public static void main(String[] args) {
		String str="my name is Kiran";
		String[] words=str.split(" ");
		//int wordLength=words.length-1;
		for(String w:words) {
			char[] c=w.toCharArray();
			for(int i=c.length-1;i>=0;i--) {
				System.out.print(c[i]);
			}
			System.out.print(" ");
		}
		
		

	}

}
