package com.enums;

public enum Cereals {
	
	CAPTAIN_CRUNCH(80),
	FROOT_LOOPS(50),
	REESES_PUFFS(95),
	COCOA_PUFFS(90);
	
	final int levelOfDeliciousness;

	private Cereals(int levelOfDeliciousness) {
		this.levelOfDeliciousness = levelOfDeliciousness;
	}
	

}
