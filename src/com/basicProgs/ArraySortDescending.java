package com.basicProgs;

public class ArraySortDescending {

	public static void main(String[] args) {
		int arr[]= {8,1,0,4,3,5,7,2};
		for(int i=0;i<=arr.length-1;i++) {
			for(int j=i+1;j<=arr.length-1;j++) {
				if(arr[i]<arr[j]) {
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
			System.out.println(arr[i]);
		}

	}

}
