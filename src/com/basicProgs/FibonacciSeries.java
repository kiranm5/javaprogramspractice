package com.basicProgs;

public class FibonacciSeries {

	public static void main(String[] args) {
		int f1=0;
		int f2=1;
		System.out.println(f1);
		System.out.println(f2);
		int fnext;
		int n=10;
		for(int i=0;i<n-2;i++) {
			fnext=f1+f2;
			System.out.println(fnext);
			f1=f2;
			f2=fnext;
		}

	}

}
