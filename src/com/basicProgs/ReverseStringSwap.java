package com.basicProgs;

public class ReverseStringSwap {

	public static void main(String[] args) {
		String str="Stevee";
		char[] ch=str.toCharArray();
		int charLen=ch.length-1;
		System.out.println(charLen);
		int left=0;
		int right=charLen;
		while(left!=right&&left<=right) {
			char temp=ch[left];
			ch[left]=ch[right];
			ch[right]=temp;
			right--;
			left++;
		}
		System.out.println(ch);
	}

}
