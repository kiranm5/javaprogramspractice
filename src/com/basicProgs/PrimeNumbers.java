package com.basicProgs;

public class PrimeNumbers {

	public static void main(String[] args) {
		int n=9;
		boolean isPrime=false;
		for(int i=2;i<n/2;i++) {
			if(n%i!=0) {
				isPrime=true;
			}
			else {
				isPrime=false;
				break;
			}
		}
		if(isPrime) {
			System.out.println("Prime number");
		}
		else {
			System.out.println("Not a Prime number");
		}
				
	}

}
