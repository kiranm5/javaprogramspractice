package com.basicProgs;

public class MaxNumArray {
	
	public static void main(String[] args) {
		int arr[]= {5,3,2,1,8,6,9};
		int len=arr.length-1;
		int max=arr[0];
		for(int i=0;i<=len;i++) {
			if(arr[i]>max) {
				max=arr[i];
			}
		}
		System.out.println(max);
	}
}
