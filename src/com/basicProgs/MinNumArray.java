package com.basicProgs;

public class MinNumArray {

	public static void main(String[] args) {
		int arr[]= {6,4,3,2,5,7,1};
		int len=arr.length-1;
		int min=arr[0];
		for(int i=0;i<=len;i++) {
			if(arr[i]<min) {
				min=arr[i];
			}
		}
		System.out.println(min);
	}

}
