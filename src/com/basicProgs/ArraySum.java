package com.basicProgs;

public class ArraySum {

	public static void main(String[] args) {
		int arr[]= {4,9,2,3,6,8,1};
		int len = arr.length-1;
		int finalSum = sumArray(arr,len);
		System.out.println("Sum of array :"+finalSum);
	}

	private static int sumArray(int[] arr, int len) {
		int sum=0;
		for(int i=0;i<=len;i++) {
			sum+=arr[i];
		}
		return sum;
	}
}
