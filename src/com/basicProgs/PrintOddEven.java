package com.basicProgs;

public class PrintOddEven {

	public static void main(String[] args) {
		int arr[]= {1,2,3,4,5,6,7,8,9};
		int len=arr.length-1;
		System.out.println("Even number");
		for(int i=0;i<=len;i++) {
			if(arr[i]%2==0) {
				System.out.println(arr[i]);
			}
		}
		System.out.println("Odd number");
		for(int i=0;i<=len;i++) {
			if(arr[i]%2==1) {
				System.out.println(arr[i]);
			}
		}

	}

}
