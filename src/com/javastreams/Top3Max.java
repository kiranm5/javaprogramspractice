package com.javastreams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Top3Max {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> ls=Arrays.asList(1,2,3,9,8,7,4,5,6);
		ls.stream().sorted(Comparator.reverseOrder()).limit(3).forEach(System.out::println);
	}

}
