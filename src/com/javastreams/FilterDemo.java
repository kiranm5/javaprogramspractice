package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class FilterDemo {

	public static void main(String[] args) {
		List<String> words=Arrays.asList("kiran","bangalore",null,"India","Karnataka");
		words.stream().filter(n->n!=null).forEach(System.out::println);

	}

}
