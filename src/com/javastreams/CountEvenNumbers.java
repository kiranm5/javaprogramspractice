package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CountEvenNumbers {

	public static void main(String[] args) {
		List<Integer> l=Arrays.asList(1,2,3,4,5,6,7,8,9);
		long result=l.stream().filter(n->n%2==0).count();
		System.out.println(result);

	}

}
