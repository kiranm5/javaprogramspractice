package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class AverageNumbers {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(3,4,5,3,2,3,4,5,5);
		int result=ls.stream().mapToInt(Integer::intValue).sum();
		System.out.println(result/2);

	}

}
