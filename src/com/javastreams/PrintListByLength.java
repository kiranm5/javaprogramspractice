package com.javastreams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PrintListByLength {

	public static void main(String[] args) {
		List<String> ls=Arrays.asList("kiran","steve","Tim","Johnson");
		ls.stream().sorted(Comparator.comparing(String::length)).forEach(System.out::println);

	}

}
