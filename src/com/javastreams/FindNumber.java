package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class FindNumber {

	public static void main(String[] args) {
		List<String> ls=Arrays.asList("7kiran","kiran");
		ls.stream().filter(s->Character.isDigit(s.charAt(0))).forEach(System.out::println);

	}

}
