package com.javastreams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Product{
	int id;
	String name;
	double price;
	public Product(int id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
}

public class FilterDemo1 {

	public static void main(String[] args) {
		List<Product> productList=new ArrayList<>();
		productList.add(new Product(1,"HP",10000));
		productList.add(new Product(2,"Dell",20000));
		productList.add(new Product(3,"Lenevo",30000));
		
		productList.stream().filter(n->n.price>25000).forEach(n->System.out.println(n.price));

	}

}
