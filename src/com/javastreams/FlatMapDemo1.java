package com.javastreams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Student{
	String sname;
	int sid;
	char grade;
	public Student(String sname, int sid, char grade) {
		super();
		this.sname = sname;
		this.sid = sid;
		this.grade = grade;
	}
	
}
public class FlatMapDemo1 {

	public static void main(String[] args) {
		List<Student> ls1=new ArrayList<Student>();
		ls1.add(new Student("kiran",3,'A'));
		ls1.add(new Student("tim",4,'A'));
		List<Student> ls2=new ArrayList<Student>();
		ls1.add(new Student("Tom",1,'B'));
		ls1.add(new Student("John",2,'B'));
		List<List<Student>> commonList=Arrays.asList(ls1,ls2);
		List<String> names=commonList.stream().flatMap(stu->stu.stream()).map(s->s.sname).collect(Collectors.toList());
		System.out.println(names.toString());
	}

}
