package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MinNumber {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(4,3,6,4,6,6,4,4,63,5,2);
		Optional<Integer> o=ls.stream().min((val1,val2)->{return val1.compareTo(val2);});
		System.out.println(o.get());

	}

}
