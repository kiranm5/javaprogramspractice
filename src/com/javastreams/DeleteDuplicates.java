package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class DeleteDuplicates {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(1,4,2,4,5,3,2,4,1,5);
		ls.stream().distinct().forEach(System.out::println);

	}

}
