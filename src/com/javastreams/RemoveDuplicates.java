package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class RemoveDuplicates {

	public static void main(String[] args) {
		List<Integer> num1=Arrays.asList(2,3);
		List<Integer> num2=Arrays.asList(4,5);
		List<Integer> num3=Arrays.asList(6,7);
		
		List<List<Integer>> finalList=Arrays.asList(num1,num2,num3);
		
		finalList.stream().flatMap(x->x.stream()).map(n->n+10).forEach(System.out::println);
		

	}

}
