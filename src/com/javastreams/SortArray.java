package com.javastreams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SortArray {

	public static void main(String[] args) {
		List<Integer> l=Arrays.asList(9,4,3,2,6,1,5);
		l.stream().sorted().forEach(System.out::println);//Ascending
		l.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);

	}

}
