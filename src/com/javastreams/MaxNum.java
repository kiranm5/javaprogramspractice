package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MaxNum {

	public static void main(String[] args) {
		List<Integer> l=Arrays.asList(4,5,6,3,2,1,6,6,8,8,99);
		Optional<Integer> o=l.stream().max((val1,val2)->{return val1.compareTo(val2);});
		System.out.println(o.get());
	}

}
