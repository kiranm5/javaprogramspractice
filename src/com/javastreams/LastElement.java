package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class LastElement {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(3,4,5,2,1,6,7,8,9,5);
		Optional<Integer> n=ls.stream().skip(ls.size()-1).findFirst();
		System.out.println(n.get());

	}

}
