package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class SumList {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(3,4,6,7,8,9,4);
		int sum=ls.stream().mapToInt(Integer::intValue).sum();
		System.out.println(sum);

	}

}
