package com.javastreams;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PrintNames {

	public static void main(String[] args) {
		List<String> ls=Arrays.asList("Kiran","Johnson","Steve","Adam");
		List longNames=ls.stream().filter(n->n.length()>5&&n.length()<=8).collect(Collectors.toList());
		System.out.println(longNames);
	}

}