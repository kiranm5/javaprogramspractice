package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class ToArray {

	public static void main(String[] args) {
		List<String> l=Arrays.asList("A","B","C","1","2","3");
		Object[] arr=l.stream().toArray();
		for(Object a:arr) {
			System.out.println(a);
		}
		
		

	}

}
