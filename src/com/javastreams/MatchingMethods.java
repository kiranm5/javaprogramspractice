package com.javastreams;

import java.util.HashSet;
import java.util.Set;

public class MatchingMethods {

	public static void main(String[] args) {
		Set<String> s=new HashSet<>();
		s.add("kiran");
		s.add("karan");
		s.add("kevin");
		boolean b=s.stream().anyMatch(str->{return str.startsWith("ki");});
		System.out.println(b);
		
		boolean boo=s.stream().allMatch(str->{return str.startsWith("ki");});
		System.out.println(boo);
		
		boolean bool=s.stream().noneMatch(str->{return str.startsWith("kirr");});
		System.out.println(bool);
		

	}

}
