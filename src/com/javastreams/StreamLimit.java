package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class StreamLimit {

	public static void main(String[] args) {
		List<Integer> l=Arrays.asList(4,5,6,3,2,9,8,7);
		l.stream().limit(3).forEach(System.out::println);

	}

}
