package com.javastreams;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class PrintEvenNumbers {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(10,11,23,18,16);
		ls.stream().filter(n->n%2==0).forEach(System.out::println);

	}

}
