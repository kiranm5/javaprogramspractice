package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringLength {

	public static void main(String[] args) {
		List<String> ls=Arrays.asList("car","bus","train","flight","bicycle");
		ls.stream().map(n->n.length()).forEach(System.out::println);;

	}

}
