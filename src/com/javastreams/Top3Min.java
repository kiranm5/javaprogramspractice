package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class Top3Min {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> ls=Arrays.asList(9,3,4,1,2,5,6,8,7);
		ls.stream().sorted().limit(3).forEach(System.out::println);

	}

}
