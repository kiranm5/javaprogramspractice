package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindAnyFindFirst {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(3,4,2,8,6,9);
		Optional ele=ls.stream().findAny();
		System.out.println(ele.get());
		Optional ele1=ls.stream().findFirst();
		System.out.println(ele1.get());

	}

}
