package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MultiplyArray {

	public static void main(String[] args) {
		List<Integer> n=Arrays.asList(2,3,4,5,6,7,8,9);
		List<Integer> result=n.stream().map(num->num*3).collect(Collectors.toList());
		System.out.println(result.toString());

	}

}
