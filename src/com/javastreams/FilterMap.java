package com.javastreams;

import java.util.Arrays;
import java.util.List;

class Employee{
	public int id;
	public String name;
	public double salary;
	public Employee(int id, String name, double salary) {
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
}
public class FilterMap {

	public static void main(String[] args) {
		List<Employee> ls=Arrays.asList(new Employee(1,"kiran",10000),
				new Employee(2,"steve",20000),
				new Employee(3,"Tim",30000),
				new Employee(4,"David",40000),
				new Employee(5,"Tom",50000)
				);
		ls.stream().filter(n->n.salary>20000).map(n->n.salary).forEach(System.out::println);
				

	}

}
