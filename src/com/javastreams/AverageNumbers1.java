package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class AverageNumbers1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> ls=Arrays.asList(3,4,5,3,2,3,4,5,5);
		int result=ls.stream().mapToInt(Integer::intValue).sum();
		System.out.println(result);
	}

}
