package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class ConvetToUpperCase {

	public static void main(String[] args) {
		List<String> ls=Arrays.asList("car","bus","train","flight","bicycle");
		ls.stream().map(n->n.toUpperCase()).forEach(System.out::println);

	}

}
