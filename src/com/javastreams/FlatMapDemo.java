package com.javastreams;

import java.util.Arrays;
import java.util.List;

public class FlatMapDemo {

	public static void main(String[] args) {
		List<String> teama=Arrays.asList("Steve","Tim");
		List<String> teamb=Arrays.asList("Tom","David");
		List<String> teamc=Arrays.asList("Johnson","Peter");
		
		List<List<String>> worldcupPlayers=Arrays.asList(teama,teamb,teamc);
		worldcupPlayers.stream().flatMap(p->p.stream()).forEach(System.out::println);

	}

}
