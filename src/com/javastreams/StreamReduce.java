package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StreamReduce {

	public static void main(String[] args) {
		List<String> ls=Arrays.asList("A","B","C","D");
		Optional<String> o=ls.stream().reduce((value,combinedValue)->{return combinedValue+value;});
		System.out.println(o.get());
	}

}
