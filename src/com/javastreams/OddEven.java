package com.javastreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OddEven {

	public static void main(String[] args) {
		List<Integer> l=Arrays.asList(2,3,4,5,6,7,8,9,10);
		System.out.println("Even numbers");
		l.stream().filter(n->n%2==0).forEach(System.out::println);
		System.out.println("Odd numbers");
		l.stream().filter(n->n%2==1).forEach(System.out::println);

	}

}
