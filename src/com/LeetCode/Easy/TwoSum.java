package com.LeetCode.Easy;

public class TwoSum {

	public static void main(String[] args) {
		int arr[]= {1,3,4,5};
		int target=9;
		int index[]=twoSum(arr,target);
		for(int i=0;i<index.length;i++) {
			System.out.print(index[i]+" ");
		}

}

	private static int[] twoSum(int[] arr, int target) {
		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++) {
				if(arr[i]+arr[j]==target) {
					return new int[] {i,j};
				}
			}
		}
		return null;
	}

}