package com.LeetCode.Easy;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CharacterCount {

	public static void main(String[] args) {
		String str="Bangalore";
		str=str.toLowerCase();
		char[] ch=str.toCharArray();
		Map<Character,Integer> hm=new LinkedHashMap<Character,Integer>();
		for(char c:ch) {
			if(hm.containsKey(c)) {
				hm.put(c,hm.get(c)+1);
			}
			else {
				hm.put(c, 1);
			}
		}
		
		for(Map.Entry entry:hm.entrySet()) {
			System.out.println(entry.getKey()+":"+entry.getValue());
		}

	}

}
