package com.LeetCode.Easy;

public class MonotonicArray {

	public static void main(String[] args) {
		
		int a[]= {5,4,3,2,1};
		boolean status=checkArray(a);
		System.out.println(status);
	}
	public static boolean checkArray(int[] n) {
		boolean increasing=true;
		boolean decreasing=true;
		
		for(int i=1;i<=n.length-1;i++) {
			increasing&=n[i]>=n[i-1];
			decreasing&=n[i]<=n[i-1];
		}
		return increasing||decreasing;
	}

}
