package com.LeetCode.Easy;

public class PalindromeNumber {

	public static void main(String[] args) {
		int num= 121;
		int rev=0;
		int rem;
		int temp=num;
		while(num!=0) {
			rem=num%10;
			rev=rev*10+rem;
			num=num/10;
		}
		if(rev!=temp) {
			System.out.println("Number is not a Palindrome");
		}
		else {
			System.out.println("Number is Palindrome");
		}

	}

}
