package com.regularExpressions;

import java.util.StringTokenizer;

public class StringTokenizer2 {

	public static void main(String[] args) {
		StringTokenizer st=new StringTokenizer("23-12-1991","-");
		while(st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}

	}

}
