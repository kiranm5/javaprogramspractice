package com.regularExpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MobileNumber {

	public static void main(String[] args) {
		Pattern p=Pattern.compile("(0|91)?[7-9][0-9]{9}");
		Matcher m=p.matcher("919845984598");
		while(m.find()) {
			System.out.println(m.start()+" "+m.group());
		}
	}

}
