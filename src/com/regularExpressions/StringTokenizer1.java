package com.regularExpressions;

import java.util.StringTokenizer;

public class StringTokenizer1 {

	public static void main(String[] args) {
		StringTokenizer st=new StringTokenizer("Bangalore Karnataka");
		while(st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}

	}

}
