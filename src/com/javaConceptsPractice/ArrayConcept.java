package com.javaConceptsPractice;

public class ArrayConcept {

	public static void main(String[] args) {
		
		int i[]=new int[4];
		i[0]=10;
		i[1]=20;
		i[2]=30;
		i[3]=40;
		System.out.println(i[1]);
		System.out.println(i[2]);
		System.out.println(i.length);
		for(int j=0;j<i.length;j++) {
			System.out.println(i[j]);
		}
		double d[]=new double[3];
		d[0]=12.33;
		d[1]=13.44;
		d[2]=45.55;
		System.out.println(d[2]);
		char ch[]=new char[3];
		ch[0]='q';
		ch[1]='2';
		ch[2]='$';
		System.out.println(ch[2]);
		
		boolean b[]= new boolean[2];
		b[0]=true;
		b[1]=false;
		System.out.println(b[1]);
		
		String s[]=new String[3];
		s[0]="Hello";
		s[1]="World";
		System.out.println(s.length);
		System.out.println(s[0]);
		
		Object ob[]=new Object[6];
		ob[0]="Tom";
		ob[1]=25;
		ob[2]=12.33;
		ob[3]=1/1/1990;
		ob[4]='T';
		ob[5]="Bangalore";
		System.out.println(ob[5]);
		
		

	}

}
