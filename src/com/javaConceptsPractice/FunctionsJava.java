package com.javaConceptsPractice;

public class FunctionsJava {

	public static void main(String[] args) {
		FunctionsJava obj=new FunctionsJava();
		obj.test();
		int sum=obj.pqr();
		System.out.println(sum);
		String s1=obj.qa();
		System.out.println(s1);
		int div=obj.division(6, 2);
		System.out.println(div);
		

	}
	
	public void test() {
		System.out.println("Test Method");
	}
	
	public int pqr() {
		System.out.println("PQR method");
		int a=10;
		int b=20;
		int c=a+b;
		return c;
	}
	
	public String qa() {
		System.out.println("QA method");
		String s="Selenium";
		return s;
	}
	
	public int division(int x,int y) {
		System.out.println("Division method");
		int d=x/y;
		return d;
		
	}
	

}
