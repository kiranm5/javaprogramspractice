package com.javaConceptsPractice;

public class TwoDimArray {

	public static void main(String[] args) {
		String x[][]=new String[3][5];
		System.out.println(x.length);//Prints row length
		System.out.println(x[0].length);//Prints column length
		
		x[0][0]="A";
		x[0][1]="B";
		x[0][2]="C";
		x[0][3]="D";
		x[0][4]="E";
		
		System.out.println(x[1][2]);
		System.out.println(x[2][2]);
		
		for(int row=0;row<x.length;row++) {
			for(int col=0;col<x[0].length;col++) {
				System.out.println(x[row][col]);
			}
		}
		

	}

}
