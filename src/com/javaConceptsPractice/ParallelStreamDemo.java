package com.javaConceptsPractice;

import java.util.Arrays;
import java.util.List;

public class ParallelStreamDemo {

	public static void main(String[] args) {
		List<Integer> ls=Arrays.asList(4,5,6,3,2,9,8);
		ls.parallelStream().map(n->n+1).forEach(System.out::println);

	}

}
