package com.javaStrings;

import java.util.Arrays;

public class LongestPrefix {

	public static void main(String[] args) {
		String[] s= new String[]{"club","clap","clove","count"};
		Arrays.sort(s);
		StringBuilder sb=new StringBuilder();
		int len=s.length-1;
		char[] start=s[0].toCharArray();
		char[] end=s[len].toCharArray();
		for(int i=0;i<=start.length-1;i++) {
			if(start[i]!=end[i]) {
				break;
			}
			sb.append(start[i]);
		}
		System.out.println(sb.toString());
	}

}
